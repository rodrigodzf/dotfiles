# Setup fzf
# ---------
if [[ ! "$PATH" == */home/diaz/.fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/diaz/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/diaz/.fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "/home/diaz/.fzf/shell/key-bindings.zsh"