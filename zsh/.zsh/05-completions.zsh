# Taken partially from
# https://dustri.org/b/my-zsh-configuration.html
# https://github.com/zsh-users/zsh-completions

zstyle ':completion:*:make:*:targets' call-command true  # fix make complete
zstyle ':completion:*:make::' tag-order targets          # fix make complete
zstyle ':completion:*' group-name ''
zstyle ':completion:*:descriptions' format '%B%d%b'

zstyle ':completion:*:processes' command 'ps -au$USER'
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*' force-list always
zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=29=34"
zstyle ':completion:*:*:killall:*' menu yes select
zstyle ':completion:*:killall:*' force-list always