you need nvim

see https://github.com/neovim/neovim/wiki/Installing-Neovim

you need to download vim-plug:

```bash
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

see https://github.com/junegunn/vim-plug