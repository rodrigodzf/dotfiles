# dotfiles

Please install oh-my-zsh and starship before running the install script.

```
cd
git clone git@gitlab.com:rodrigodzf/dotfiles.git
cd dotfiles
stow tmux
stow zsh
stow git
```

To work properly tmux needs fzf and neovim

neovim can be installed with:
```
curl -L https://github.com/neovim/neovim/releases/download/nightly/nvim.appimage > /tmp/nvim.appimage
sudo mv /tmp/nvim.appimage /usr/local/bin/nvim
chmod +x /usr/local/bin/nvim
```

then clone tpm (https://github.com/tmux-plugins/tpm)